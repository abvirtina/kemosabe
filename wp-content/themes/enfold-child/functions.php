<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

/**
 * Google Tag Manager Body Script
 */
function google_tag_manager_body_code() { ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTPPXBF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) —>

<?php }
add_action( 'wp_footer', 'google_tag_manager_body_code', 1);

/**
 * Google Tag Manager Head Script
 */
function google_tag_manager_head_code(){ ?>
   
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTPPXBF');</script>


<!-- End Google Tag Manager -->

<?php }
add_action('wp_head', 'google_tag_manager_head_code', 1);

add_filter('avia_post_nav_settings', 'avia_post_nav_settings_mod');
function avia_post_nav_settings_mod($settings)
{
  if(is_singular('product')) {
        $settings['taxonomy'] = 'product_cat';
	$settings['is_fullwidth'] = false;
  }
  $settings['same_category'] = true;
  return $settings;
}
add_filter('avf_form_from', 'avf_form_from_mod', 10, 3);
function avf_form_from_mod($from, $new_post, $form_params) {
    $from = " woodrow@kemosabe.com ";
    return $from;
}

add_filter( 'woocommerce_product_tabs', 'kemosabe_remove_product_tabs', 98 );
 
function kemosabe_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] ); 
    return $tabs;
}



function cartjs() {
  //wp_enqueue_script( 'jqueryscript', get_bloginfo( 'stylesheet_directory' ) . '/js/jquery-1.10.2.js', array( 'jquery' ), '1.0.0' );
    
}
add_action( 'wp_enqueue_scripts', 'cartjs' );

/*
add_filter( 'woocommerce_rest_pre_insert_product_object', 'lpb_wc_disable_magage_stock_in_rest_api', 10, 2 );
function lpb_wc_disable_magage_stock_in_rest_api ( $product, $request ) {

    
    $product = wc_get_product( $request['id'] );

   
    // Post title.
    if ( isset( $request['name'] ) ) {      
      $product->set_name( wp_filter_post_kses($product->name ));      
    }

    // Post content.
    if ( isset( $request['description'] ) ) {
      $product->set_description( wp_filter_post_kses( $product->description ) );
      //$product->set_description( wp_filter_post_kses( $request['description'] ) );
    }

    // Post excerpt.
    if ( isset( $request['short_description'] ) ) {
      $product->set_short_description( wp_filter_post_kses( $product->short_description ) );
    }

    // Post status.
    if ( isset( $request['status'] ) ) {
      $product->set_status( $product->status );
    }
    
    // Post slug.
    if ( isset( $request['slug'] ) ) {
      $product->set_slug( $product->slug );
    }

    // Menu order.
    if ( isset( $request['menu_order'] ) ) {
      $product->set_menu_order( $product->menu_order );
    }

    // Comment status.
    if ( isset( $request['reviews_allowed'] ) ) {
      $product->set_reviews_allowed( $product->reviews_allowed );
    }

    // Virtual.
    if ( isset( $request['virtual'] ) ) {
      $product->set_virtual( $product->virtual );
    }

    // Tax status.
    if ( isset( $request['tax_status'] ) ) {
      $product->set_tax_status( $product->tax_status );
    }
    // Tax Class.
    if ( isset( $request['tax_class'] ) ) {
      $product->set_tax_class( $product->tax_class );
    }

    // Catalog Visibility.
    if ( isset( $request['catalog_visibility'] ) ) {
      $product->set_catalog_visibility( $product->catalog_visibility );
    }

    // Purchase Note.
    if ( isset( $request['purchase_note'] ) ) {
      $product->set_purchase_note( wp_kses_post( wp_unslash( $product->purchase_note ) ) );
    }
    

    // Featured Product.
    if ( isset( $request['featured'] ) ) {
      $product->set_featured( $product->featured );
    }

    // SKU.
    if ( isset( $request['sku'] ) ) {
      $product->set_sku( wc_clean( $product->sku ) );
    }
    // Attributes.
    if ( isset( $request['attributes'] ) ) {
      $product->set_attributes( $product->attributes );
    } 



    // Sales and prices.
    if ( in_array( $product->get_type(), array( 'variable', 'grouped' ), true ) ) {
      $product->set_regular_price( '' );
      $product->set_sale_price( '' );
      $product->set_date_on_sale_to( '' );
      $product->set_date_on_sale_from( '' );
      $product->set_price( '' );
    } else {
      // Regular Price.
      if ( isset( $request['regular_price'] ) ) {
        $product->set_regular_price( $product->regular_price );
      }

      // Sale Price.
      if ( isset( $request['sale_price'] ) ) {
        $product->set_sale_price( $product->sale_price );
      }

      if ( isset( $request['date_on_sale_from'] ) ) {
        $product->set_date_on_sale_from( $product->date_on_sale_from );
      }

      if ( isset( $request['date_on_sale_from_gmt'] ) ) {
        $product->set_date_on_sale_from(  $product->date_on_sale_from_gmt );
      }

      if ( isset( $request['date_on_sale_to'] ) ) {
        $product->set_date_on_sale_to(  $product->date_on_sale_to );
      }

      if ( isset( $request['date_on_sale_to_gmt'] ) ) {
        $product->set_date_on_sale_to( $product->date_on_sale_to_gmt );
      }
    }

    // Product parent ID.
    if ( isset( $request['parent_id'] ) ) {
      $product->set_parent_id( $product->parent_id );
    }

    // Sold individually.
    if ( isset( $request['sold_individually'] ) ) {
      $product->set_sold_individually( $product->sold_individually );
    }

    // Stock status.
    if ( isset( $request['in_stock'] ) ) {
      $stock_status = true === $request['in_stock'] ? 'instock' : 'outofstock';
    } else {
      $stock_status = $product->get_stock_status();
    }

    // Stock data.
    if ( 'yes' === get_option( 'woocommerce_manage_stock' ) ) {
      // Manage stock.
      if ( isset( $request['manage_stock'] ) ) {
        $product->set_manage_stock( $request['manage_stock'] );
      }

      // Backorders.
      if ( isset( $request['backorders'] ) ) {
        $product->set_backorders( $request['backorders'] );
      }

      if ( $product->is_type( 'grouped' ) ) {
        $product->set_manage_stock( 'no' );
        $product->set_backorders( 'no' );
        $product->set_stock_quantity( '' );
        $product->set_stock_status( $stock_status );
      } elseif ( $product->is_type( 'external' ) ) {
        $product->set_manage_stock( 'no' );
        $product->set_backorders( 'no' );
        $product->set_stock_quantity( '' );
        $product->set_stock_status( 'instock' );
      } elseif ( $product->get_manage_stock() ) {
        // Stock status is always determined by children so sync later.
        if ( ! $product->is_type( 'variable' ) ) {
          $product->set_stock_status( $stock_status );
        }

        // Stock quantity.
        if ( isset( $request['stock_quantity'] ) ) {
          $product->set_stock_quantity( wc_stock_amount( $request['stock_quantity'] ) );
        } elseif ( isset( $request['inventory_delta'] ) ) {
          $stock_quantity  = wc_stock_amount( $product->get_stock_quantity() );
          $stock_quantity += wc_stock_amount( $request['inventory_delta'] );
          $product->set_stock_quantity( wc_stock_amount( $stock_quantity ) );
        }
      } else {
        // Don't manage stock.
        $product->set_manage_stock( 'no' );
        $product->set_stock_quantity( '' );
        $product->set_stock_status( $stock_status );
      }
    } elseif ( ! $product->is_type( 'variable' ) ) {
      $product->set_stock_status( $stock_status );
    }


        // Upsells.
    if ( isset( $request['upsell_ids'] ) ) {
      $upsells = array();
      $ids     = $product['upsell_ids'];

      if ( ! empty( $ids ) ) {
        foreach ( $ids as $id ) {
          if ( $id && $id > 0 ) {
            $upsells[] = $id;
          }
        }
      }

      $product->set_upsell_ids( $upsells );
    }

    // Cross sells.
    if ( isset( $request['cross_sell_ids'] ) ) {
      $crosssells = array();
      $ids        = $product['cross_sell_ids'];

      if ( ! empty( $ids ) ) {
        foreach ( $ids as $id ) {
          if ( $id && $id > 0 ) {
            $crosssells[] = $id;
          }
        }
      }

      $product->set_cross_sell_ids( $crosssells );
    }

  // Product categories.
    if ( isset( $request['categories'] ) && is_array( $request['categories'] ) ) {
      $product =$pv2cobject->save_taxonomy_terms2( $product, $product['categories'] );
    }

      $pv2cobject= new WC_REST_Products_V2_Controller();
  // Product tags.
    if ( isset( $request['tags'] ) && is_array( $request['tags'] ) ) {
      $product =$pv2cobject->save_taxonomy_terms( $product, $product['tags'], 'tag' );
    }

    // Downloadable.
    if ( isset( $request['downloadable'] ) ) {
      $product->set_downloadable( $product->downloadable );
    }

    
    // Downloadable options.
    if ( $product->get_downloadable() ) {

      // Downloadable files.
      if ( isset( $request['downloads'] ) && is_array( $request['downloads'] ) ) {
        $product = $pv2cobject->save_downloadable_files( $product, $product['downloads'] );
      }

      // Download limit.
      if ( isset( $request['download_limit'] ) ) {
        $product->set_download_limit( $product['download_limit'] );
      }

      // Download expiry.
      if ( isset( $request['download_expiry'] ) ) {
        $product->set_download_expiry( $product['download_expiry'] );
      }
    }

    // Product url and button text for external products.
    if ( $product->is_type( 'external' ) ) {
      if ( isset( $request['external_url'] ) ) {
        $product->set_product_url( $product->external_url );
      }

      if ( isset( $request['button_text'] ) ) {
        $product->set_button_text( $product->button_text );
      }
    }

  
    // Save default attributes for variable products.
    if ( $product->is_type( 'variable' ) ) {
      $product = $pv2cobject->save_default_attributes( $product, $product );
    }
    // Set children for a grouped product.
    if ( $product->is_type( 'grouped' ) && isset( $request['grouped_products'] ) ) {
      $product->set_children( $product->grouped_products );
    }
    // Check for featured/gallery images, upload it and set it.
    if ( isset( $request['images'] ) ) {
      $product = $pv2cobject->set_product_images( $product, $product['images'] );
    }

    // Allow set meta_data.
    if ( is_array( $request['meta_data'] ) ) {
      foreach ( $product['meta_data'] as $meta ) {
        $product->update_meta_data( $meta['key'], $meta['value'], isset( $meta['id'] ) ? $meta['id'] : '' );
      }
    }
  return $product;
}

add_filter( 'woocommerce_rest_pre_insert_product_variation_object', 'lpb_wc_disable_except_magage_stock_in_rest_api_variable', 10, 2 );

function lpb_wc_disable_except_magage_stock_in_rest_api_variable ( $variation, $request ) {

    $product = wc_get_product( $request['id'] );
   
  
 //if ( $product->is_type( 'simple' ) && $product->is_type( 'variable' ) ) {

    // Post title.
    if ( isset( $request['name'] ) ) {
      
      $product->set_name( wp_filter_post_kses($product->name ));
      
    }

    // Post content.
    if ( isset( $request['description'] ) ) {
      $product->set_description( wp_filter_post_kses( $product->description ) );
      //$product->set_description( wp_filter_post_kses( $request['description'] ) );
    }

    // Post excerpt.
    if ( isset( $request['short_description'] ) ) {
      $product->set_short_description( wp_filter_post_kses( $product->short_description ) );
    }

    // Post status.
    if ( isset( $request['status'] ) ) {
      $product->set_status( $product->status );
    }
    
    // Post slug.
    if ( isset( $request['slug'] ) ) {
      $product->set_slug( $product->slug );
    }

    // Menu order.
    if ( isset( $request['menu_order'] ) ) {
      $product->set_menu_order( $product->menu_order );
    }

    // Comment status.
    if ( isset( $request['reviews_allowed'] ) ) {
      $product->set_reviews_allowed( $product->reviews_allowed );
    }

    // Virtual.
    if ( isset( $request['virtual'] ) ) {
      $product->set_virtual( $product->virtual );
    }

    // Tax status.
    if ( isset( $request['tax_status'] ) ) {
      $product->set_tax_status( $product->tax_status );
    }
    // Tax Class.
    if ( isset( $request['tax_class'] ) ) {
      $product->set_tax_class( $product->tax_class );
    }

    // Catalog Visibility.
    if ( isset( $request['catalog_visibility'] ) ) {
      $product->set_catalog_visibility( $product->catalog_visibility );
    }

    // Purchase Note.
    if ( isset( $request['purchase_note'] ) ) {
      $product->set_purchase_note( wp_kses_post( wp_unslash( $product->purchase_note ) ) );
    }
    

    // Featured Product.
    if ( isset( $request['featured'] ) ) {
      $product->set_featured( $product->featured );
    }

    // SKU.
    if ( isset( $request['sku'] ) ) {
      $product->set_sku( wc_clean( $product->sku ) );
    }
    // Attributes.
    if ( isset( $request['attributes'] ) ) {
      $product->set_attributes( $product->attributes );
    } 



    // Sales and prices.
    if ( in_array( $product->get_type(), array( 'variable', 'grouped' ), true ) ) {
      $product->set_regular_price( '' );
      $product->set_sale_price( '' );
      $product->set_date_on_sale_to( '' );
      $product->set_date_on_sale_from( '' );
      $product->set_price( '' );
    } else {
      // Regular Price.
      if ( isset( $request['regular_price'] ) ) {
        $product->set_regular_price( $product->regular_price );
      }

      // Sale Price.
      if ( isset( $request['sale_price'] ) ) {
        $product->set_sale_price( $product->sale_price );
      }

      if ( isset( $request['date_on_sale_from'] ) ) {
        $product->set_date_on_sale_from( $product->date_on_sale_from );
      }

      if ( isset( $request['date_on_sale_from_gmt'] ) ) {
        $product->set_date_on_sale_from(  $product->date_on_sale_from_gmt );
      }

      if ( isset( $request['date_on_sale_to'] ) ) {
        $product->set_date_on_sale_to(  $product->date_on_sale_to );
      }

      if ( isset( $request['date_on_sale_to_gmt'] ) ) {
        $product->set_date_on_sale_to( $product->date_on_sale_to_gmt );
      }
    }

    // Product parent ID.
    if ( isset( $request['parent_id'] ) ) {
      $product->set_parent_id( $product->parent_id );
    }

    // Sold individually.
    if ( isset( $request['sold_individually'] ) ) {
      $product->set_sold_individually( $product->sold_individually );
    }

    // Stock status.
    if ( isset( $request['in_stock'] ) ) {
      $stock_status = true === $request['in_stock'] ? 'instock' : 'outofstock';
    } else {
      $stock_status = $product->get_stock_status();
    }

    // Stock data.
    if ( $product->get_manage_stock() ) {
      // Manage stock.
      if ( isset( $request['manage_stock'] ) ) {
        $product->set_manage_stock( $request['manage_stock'] );
      }

      // Backorders.
      if ( isset( $request['backorders'] ) ) {
        $product->set_backorders( $request['backorders'] );
      }

      if ( $product->is_type( 'grouped' ) ) {
        $product->set_manage_stock( 'no' );
        $product->set_backorders( 'no' );
        $product->set_stock_quantity( '' );
        $product->set_stock_status( $stock_status );
      } elseif ( $product->is_type( 'external' ) ) {
        $product->set_manage_stock( 'no' );
        $product->set_backorders( 'no' );
        $product->set_stock_quantity( '' );
        $product->set_stock_status( 'instock' );
      } elseif ( $product->get_manage_stock() ) {
        // Stock status is always determined by children so sync later.
        if ( ! $product->is_type( 'variable' ) ) {
          $product->set_stock_status( $stock_status );
        }

        // Stock quantity.
        if ( isset( $request['stock_quantity'] ) ) {
          $product->set_stock_quantity( wc_stock_amount( $request['stock_quantity'] ) );
        } elseif ( isset( $request['inventory_delta'] ) ) {
          $stock_quantity  = wc_stock_amount( $product->get_stock_quantity() );
          $stock_quantity += wc_stock_amount( $request['inventory_delta'] );
          $product->set_stock_quantity( wc_stock_amount( $stock_quantity ) );
        }
      } else {
        // Don't manage stock.
        $product->set_manage_stock( 'no' );
        $product->set_stock_quantity( '' );
        $product->set_stock_status( $stock_status );
      }
    } elseif ( ! $product->is_type( 'variable' ) ) {
      $product->set_stock_status( $stock_status );
    }


        // Upsells.
    if ( isset( $request['upsell_ids'] ) ) {
      $upsells = array();
      $ids     = $product['upsell_ids'];

      if ( ! empty( $ids ) ) {
        foreach ( $ids as $id ) {
          if ( $id && $id > 0 ) {
            $upsells[] = $id;
          }
        }
      }

      $product->set_upsell_ids( $upsells );
    }

    // Cross sells.
    if ( isset( $request['cross_sell_ids'] ) ) {
      $crosssells = array();
      $ids        = $product['cross_sell_ids'];

      if ( ! empty( $ids ) ) {
        foreach ( $ids as $id ) {
          if ( $id && $id > 0 ) {
            $crosssells[] = $id;
          }
        }
      }

      $product->set_cross_sell_ids( $crosssells );
    }

  // Product categories.
    if ( isset( $request['categories'] ) && is_array( $request['categories'] ) ) {
      $product =$pv2cobject->save_taxonomy_terms2( $product, $product['categories'] );
    }

      $pv2cobject= new WC_REST_Products_V2_Controller();
  // Product tags.
    if ( isset( $request['tags'] ) && is_array( $request['tags'] ) ) {
      $product =$pv2cobject->save_taxonomy_terms( $product, $product['tags'], 'tag' );
    }

    // Downloadable.
    if ( isset( $request['downloadable'] ) ) {
      $product->set_downloadable( $product->downloadable );
    }

    
    // Downloadable options.
    if ( $product->get_downloadable() ) {

      // Downloadable files.
      if ( isset( $request['downloads'] ) && is_array( $request['downloads'] ) ) {
        $product = $pv2cobject->save_downloadable_files( $product, $product['downloads'] );
      }

      // Download limit.
      if ( isset( $request['download_limit'] ) ) {
        $product->set_download_limit( $product['download_limit'] );
      }

      // Download expiry.
      if ( isset( $request['download_expiry'] ) ) {
        $product->set_download_expiry( $product['download_expiry'] );
      }
    }

    // Product url and button text for external products.
    if ( $product->is_type( 'external' ) ) {
      if ( isset( $request['external_url'] ) ) {
        $product->set_product_url( $product->external_url );
      }

      if ( isset( $request['button_text'] ) ) {
        $product->set_button_text( $product->button_text );
      }
    }

  
    // Save default attributes for variable products.
    if ( $product->is_type( 'variable' ) ) {
      $product = $pv2cobject->save_default_attributes( $product, $product );
    }
    // Set children for a grouped product.
    if ( $product->is_type( 'grouped' ) && isset( $request['grouped_products'] ) ) {
      $product->set_children( $product->grouped_products );
    }
    // Check for featured/gallery images, upload it and set it.
    if ( isset( $request['images'] ) ) {
      $product = $pv2cobject->set_product_images( $product, $product['images'] );
    }

    // Allow set meta_data.
    if ( is_array( $request['meta_data'] ) ) {
      foreach ( $product['meta_data'] as $meta ) {
        $product->update_meta_data( $meta['key'], $meta['value'], isset( $meta['id'] ) ? $meta['id'] : '' );
      }
    }

  //} 
  return $product;
}

*/

/**
 * Custom add to wishlist button on product listing.
 *
 * @since 1.0.0
 */
function jas_claue_wc_wishlist_button_simple() {
  global $product, $yith_wcwl;

  if ( ! class_exists( 'YITH_WCWL' ) || $product->is_type( 'variable' ) ) return;

  $url          = YITH_WCWL()->get_wishlist_url();
  $product_type = $product->get_type();
  $exists       = $yith_wcwl->is_product_in_wishlist( $product->get_id() );
  $classes      = 'class="add_to_wishlist cw"';
  $add          = get_option( 'yith_wcwl_add_to_wishlist_text' );
  $browse       = get_option( 'yith_wcwl_browse_wishlist_text' );
  $added        = get_option( 'yith_wcwl_product_added_text' );

  $output = '';

  $output  .= '<div class="yith-wcwl-add-to-wishlist ts__03 mg__0 pr add-to-wishlist-' . esc_attr( $product->get_id() ) . '">';
    $output .= '<div class="yith-wcwl-add-button';
      $output .= $exists ? ' hide" style="display:none;"' : ' show"';
      $output .= '><a href="' . esc_url( htmlspecialchars( YITH_WCWL()->get_wishlist_url() ) ) . '" data-product-id="' . esc_attr( $product->get_id() ) . '" data-product-type="' . esc_attr( $product_type ) . '" ' . $classes . ' ><i class="fa fa-heart-o"></i></a>';
      $output .= '<i class="fa fa-spinner fa-pulse ajax-loading pa" style="visibility:hidden"></i>';
    $output .= '</div>';

    $output .= '<div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;"><a class="chp" href="' . esc_url( $url ) . '"><i class="fa fa-heart"></i></a></div>';
    $output .= '<div class="yith-wcwl-wishlistexistsbrowse ' . ( $exists ? 'show' : 'hide' ) . '" style="display:' . ( $exists ? 'block' : 'none' ) . '"><a href="' . esc_url( $url ) . '" class="chp"><i class="fa fa-heart"></i></a></div>';
  $output .= '</div>';

  echo $output;
}
function jas_claue_wc_wishlist_button_variable() {
  global $product, $yith_wcwl;

  if ( ! class_exists( 'YITH_WCWL' ) || ! $product->is_type( 'variable' ) ) return;

  $url          = YITH_WCWL()->get_wishlist_url();
  $product_type = $product->get_type();
  $exists       = $yith_wcwl->is_product_in_wishlist( $product->get_id() );
  $classes      = 'class="add_to_wishlist cw"';
  $add          = get_option( 'yith_wcwl_add_to_wishlist_text' );
  $browse       = get_option( 'yith_wcwl_browse_wishlist_text' );
  $added        = get_option( 'yith_wcwl_product_added_text' );

  $output = '';

  $output  .= '<div class="yith-wcwl-add-to-wishlist ts__03 mg__0 pr add-to-wishlist-' . esc_attr( $product->get_id() ) . '">';
    $output .= '<div class="yith-wcwl-add-button';
      $output .= $exists ? ' hide" style="display:none;"' : ' show"';
      $output .= '><a href="' . esc_url( htmlspecialchars( YITH_WCWL()->get_wishlist_url() ) ) . '" data-product-id="' . esc_attr( $product->get_id() ) . '" data-product-type="' . esc_attr( $product_type ) . '" ' . $classes . ' ><i class="fa fa-heart-o"></i></a>';
      $output .= '<i class="fa fa-spinner fa-pulse ajax-loading pa" style="visibility:hidden"></i>';
    $output .= '</div>';

    $output .= '<div class="yith-wcwl-wishlistaddedbrowse hide" style="display:none;"><a class="chp" href="' . esc_url( $url ) . '"><i class="fa fa-heart"></i></a></div>';
    $output .= '<div class="yith-wcwl-wishlistexistsbrowse ' . ( $exists ? 'show' : 'hide' ) . '" style="display:' . ( $exists ? 'block' : 'none' ) . '"><a href="' . esc_url( $url ) . '" class="chp"><i class="fa fa-heart"></i></a></div>';
  $output .= '</div>';

  echo $output;
}
add_action( 'woocommerce_after_add_to_cart_button', 'jas_claue_wc_wishlist_button_simple' );
add_action( 'woocommerce_after_shop_loop_item_title', 'jas_claue_wc_wishlist_button_simple', 11 );
add_action( 'woocommerce_after_single_variation', 'jas_claue_wc_wishlist_button_variable' );

/**
 * Loads the parent stylesheet.
 */
function load_parent_stylesheet() {
  wp_enqueue_style( 'styles-child', get_stylesheet_directory_uri() . '/style.css', array(), '1.0.6' );
  wp_enqueue_script( 'script-child', get_stylesheet_directory_uri() . '/assets/script.js', array(), '1.0.1', true );
  //wp_enqueue_script( 'cartjs', get_bloginfo( 'stylesheet_directory' ) . '/js/customjs.js', array( 'jquery' ), filemtime(get_template_directory().'-child/js/customjs.js'), true );
}
add_action( 'wp_enqueue_scripts', 'load_parent_stylesheet' );

add_action('woocommerce_review_order_before_payment', 'trustSeal');
function trustSeal(){
  echo '<!-- (c) 2005, 2019. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="woocommerce-info AuthorizeNetSeal" style="padding: 0; margin-top: 3em;"> <script type="text/javascript" language="javascript">var ANS_customer_id="7d2a35d0-2aa8-4cab-99c5-55d4276fe2b4";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net:443/anetseal/seal.js" ></script> </div>';
}


function add_hello_bar_text( $settings ) {

  $updated_settings = array();

  foreach ( $settings as $section ) {

    // at the bottom of the General Options section
    if ( isset( $section['id'] ) && 'general_options' == $section['id'] &&
       isset( $section['type'] ) && 'sectionend' == $section['type'] ) {

      $updated_settings[] = array(
        'name'     => __( 'Top Hello Bar Text', 'wc_seq_hello_bar' ),
        'desc_tip' => __( 'Top Hello Bar Content to show in the frontend.', 'wc_seq_hello_bar' ),
        'id'       => 'hello_bar_text',
        'type'     => 'text',
        'css'      => 'min-width:300px;',
        'std'      => '1',  // WC < 2.0
        'default'  => '',  // WC >= 2.0
        'desc'     => __( 'Top Hello Bar Content to show in the frontend.', 'wc_seq_hello_bar' ),
      );
    $updated_settings[] = array(
          'name'     => __( 'Top Hello Bar Text Display', 'wc_seq_hello_bar_display' ),
          'id'       => 'hello_bar_display',
          'type'     => 'checkbox',
          'std'      => 'yes',  // WC < 2.0
          'default'  => '0',  // WC >= 2.0
          'desc'     => __( 'Click the checkbox to show the Hello Bar.', 'wc_seq_hello_bar_display' ),
        );
    }

    $updated_settings[] = $section;
  }

  return $updated_settings;
}
add_filter( 'woocommerce_general_settings', 'add_hello_bar_text' );