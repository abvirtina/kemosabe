<?php

/* filter-menu.twig */
class __TwigTemplate_1f25bacc16e82305cef35f2d4954e3a58cf88f86b74ba3bdfdb3edd107c03a6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((!array_key_exists("hide_toolbar", $context))) {
            // line 2
            echo "\t";
            if (array_key_exists("ai1ec_before_filter_menu", $context)) {
                // line 3
                echo "\t\t";
                echo (isset($context["ai1ec_before_filter_menu"]) ? $context["ai1ec_before_filter_menu"] : null);
                echo "
\t";
            }
            // line 5
            echo "\t<div class=\"timely ai1ec-calendar-toolbar ai1ec-clearfix
\t";
            // line 6
            if (((((twig_test_empty((isset($context["categories"]) ? $context["categories"] : null)) && twig_test_empty((isset($context["tags"]) ? $context["tags"] : null))) && (!array_key_exists("additional_filters", $context))) && twig_test_empty((isset($context["contribution_buttons"]) ? $context["contribution_buttons"] : null))) && (!array_key_exists("additional_buttons", $context)))) {
                // line 12
                echo "\t\tai1ec-hidden
\t";
            }
            // line 14
            echo "\t\">
\t\t<ul class=\"ai1ec-nav ai1ec-nav-pills ai1ec-pull-left ai1ec-filters\">
\t\t\t";
            // line 16
            echo (isset($context["categories"]) ? $context["categories"] : null);
            echo "
\t\t\t";
            // line 17
            echo (isset($context["tags"]) ? $context["tags"] : null);
            echo "
\t\t\t";
            // line 18
            if (array_key_exists("additional_filters", $context)) {
                // line 19
                echo "\t\t\t\t";
                echo (isset($context["additional_filters"]) ? $context["additional_filters"] : null);
                echo "
\t\t\t";
            }
            // line 21
            echo "\t\t</ul>
\t\t<div class=\"ai1ec-pull-right\">
\t\t";
            // line 23
            if (array_key_exists("additional_buttons", $context)) {
                // line 24
                echo "\t\t\t";
                echo (isset($context["additional_buttons"]) ? $context["additional_buttons"] : null);
                echo "
\t\t";
            }
            // line 26
            echo "\t\t</div>
\t</div>";
        }
    }

    public function getTemplateName()
    {
        return "filter-menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 26,  65 => 24,  63 => 23,  59 => 21,  53 => 19,  51 => 18,  477 => 175,  474 => 174,  470 => 173,  463 => 172,  456 => 171,  454 => 170,  446 => 168,  442 => 166,  439 => 165,  436 => 164,  431 => 161,  425 => 158,  422 => 157,  419 => 156,  413 => 153,  410 => 152,  408 => 151,  404 => 149,  401 => 148,  398 => 147,  395 => 146,  392 => 145,  386 => 142,  382 => 140,  380 => 139,  377 => 138,  371 => 135,  368 => 134,  366 => 133,  362 => 132,  357 => 130,  353 => 128,  347 => 126,  342 => 123,  340 => 122,  337 => 121,  335 => 120,  331 => 119,  322 => 114,  317 => 112,  313 => 111,  309 => 109,  307 => 108,  302 => 105,  296 => 103,  294 => 102,  289 => 100,  285 => 99,  281 => 98,  276 => 96,  271 => 93,  265 => 90,  260 => 89,  257 => 88,  255 => 87,  250 => 84,  247 => 83,  243 => 81,  237 => 79,  235 => 78,  231 => 77,  226 => 76,  220 => 73,  217 => 72,  212 => 70,  208 => 69,  204 => 67,  202 => 66,  199 => 65,  194 => 62,  188 => 60,  186 => 59,  182 => 58,  178 => 57,  174 => 56,  167 => 52,  161 => 49,  158 => 48,  155 => 47,  153 => 46,  150 => 45,  144 => 43,  142 => 42,  138 => 41,  134 => 40,  130 => 39,  125 => 37,  120 => 35,  113 => 32,  107 => 30,  105 => 29,  99 => 28,  95 => 27,  91 => 26,  88 => 25,  83 => 24,  78 => 23,  73 => 22,  67 => 19,  64 => 18,  36 => 8,  33 => 6,  30 => 5,  38 => 7,  25 => 5,  52 => 12,  47 => 17,  41 => 8,  35 => 12,  27 => 4,  60 => 13,  48 => 8,  40 => 10,  34 => 5,  29 => 4,  22 => 2,  44 => 10,  28 => 4,  24 => 3,  86 => 26,  75 => 21,  70 => 19,  66 => 14,  62 => 17,  58 => 16,  54 => 15,  50 => 14,  43 => 16,  39 => 14,  31 => 5,  26 => 3,  21 => 2,  19 => 1,);
    }
}
