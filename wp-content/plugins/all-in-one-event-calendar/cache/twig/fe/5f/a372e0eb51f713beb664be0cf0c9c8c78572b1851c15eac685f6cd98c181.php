<?php

/* calendar.twig */
class __TwigTemplate_fe5fa372e0eb51f713beb664be0cf0c9c8c78572b1851c15eac685f6cd98c181 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!-- START All-in-One Event Calendar Plugin - Version ";
        echo (isset($context["version"]) ? $context["version"] : null);
        echo " -->
<div id=\"ai1ec-container\"
\t class=\"ai1ec-main-container ";
        // line 4
        echo (isset($context["ai1ec_calendar_classes"]) ? $context["ai1ec_calendar_classes"] : null);
        echo "\">
\t<!-- AI1EC_PAGE_CONTENT_PLACEHOLDER -->
\t<div id=\"ai1ec-calendar\" class=\"timely ai1ec-calendar\">
\t\t";
        // line 7
        if (array_key_exists("ai1ec_above_calendar", $context)) {
            // line 8
            echo "\t\t\t";
            echo (isset($context["ai1ec_above_calendar"]) ? $context["ai1ec_above_calendar"] : null);
            echo "
\t\t";
        }
        // line 10
        echo "\t\t";
        echo (isset($context["filter_menu"]) ? $context["filter_menu"] : null);
        echo "
\t\t<div id=\"ai1ec-calendar-view-container\"
\t\t\t class=\"ai1ec-calendar-view-container\">
\t\t\t<div id=\"ai1ec-calendar-view-loading\"
\t\t\t\t class=\"ai1ec-loading ai1ec-calendar-view-loading\"></div>
\t\t\t<div id=\"ai1ec-calendar-view\" class=\"ai1ec-calendar-view\">
\t\t\t\t";
        // line 16
        echo (isset($context["view"]) ? $context["view"] : null);
        echo "
\t\t\t</div>
\t\t</div>
\t\t<div class=\"ai1ec-subscribe-container ai1ec-pull-right ai1ec-btn-group\">
\t\t\t";
        // line 20
        echo (isset($context["subscribe_buttons"]) ? $context["subscribe_buttons"] : null);
        echo "
\t\t</div>
\t\t";
        // line 22
        echo (isset($context["after_view"]) ? $context["after_view"] : null);
        echo "
\t</div><!-- /.timely -->
</div>
";
        // line 25
        if ((!twig_test_empty((isset($context["inline_js_calendar"]) ? $context["inline_js_calendar"] : null)))) {
            // line 26
            echo "\t<script type=\"text/javascript\">";
            echo (isset($context["inline_js_calendar"]) ? $context["inline_js_calendar"] : null);
            echo "</script>
";
        }
        // line 28
        echo "<!-- END All-in-One Event Calendar Plugin -->
";
        // line 30
        echo "

";
    }

    public function getTemplateName()
    {
        return "calendar.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 26,  61 => 22,  56 => 20,  49 => 16,  71 => 26,  65 => 24,  63 => 23,  59 => 21,  53 => 19,  51 => 18,  477 => 175,  474 => 174,  470 => 173,  463 => 172,  456 => 171,  454 => 170,  446 => 168,  442 => 166,  439 => 165,  436 => 164,  431 => 161,  425 => 158,  422 => 157,  419 => 156,  413 => 153,  410 => 152,  408 => 151,  404 => 149,  401 => 148,  398 => 147,  395 => 146,  392 => 145,  386 => 142,  382 => 140,  380 => 139,  377 => 138,  371 => 135,  368 => 134,  366 => 133,  362 => 132,  357 => 130,  353 => 128,  347 => 126,  342 => 123,  340 => 122,  337 => 121,  335 => 120,  331 => 119,  322 => 114,  317 => 112,  313 => 111,  309 => 109,  307 => 108,  302 => 105,  296 => 103,  294 => 102,  289 => 100,  285 => 99,  281 => 98,  276 => 96,  271 => 93,  265 => 90,  260 => 89,  257 => 88,  255 => 87,  250 => 84,  247 => 83,  243 => 81,  237 => 79,  235 => 78,  231 => 77,  226 => 76,  220 => 73,  217 => 72,  212 => 70,  208 => 69,  204 => 67,  202 => 66,  199 => 65,  194 => 62,  188 => 60,  186 => 59,  182 => 58,  178 => 57,  174 => 56,  167 => 52,  161 => 49,  158 => 48,  155 => 47,  153 => 46,  150 => 45,  144 => 43,  142 => 42,  138 => 41,  134 => 40,  130 => 39,  125 => 37,  120 => 35,  113 => 32,  107 => 30,  105 => 29,  99 => 28,  95 => 27,  91 => 26,  88 => 25,  83 => 24,  78 => 30,  73 => 22,  67 => 25,  64 => 18,  36 => 8,  33 => 8,  30 => 5,  38 => 7,  25 => 4,  52 => 12,  47 => 17,  41 => 8,  35 => 12,  27 => 4,  60 => 13,  48 => 8,  40 => 10,  34 => 5,  29 => 4,  22 => 2,  44 => 10,  28 => 4,  24 => 3,  86 => 26,  75 => 28,  70 => 19,  66 => 14,  62 => 17,  58 => 16,  54 => 15,  50 => 14,  43 => 16,  39 => 10,  31 => 7,  26 => 3,  21 => 2,  19 => 2,);
    }
}
