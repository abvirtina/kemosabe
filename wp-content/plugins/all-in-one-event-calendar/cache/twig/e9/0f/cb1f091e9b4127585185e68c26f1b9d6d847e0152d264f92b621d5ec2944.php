<?php

/* posterboard.twig */
class __TwigTemplate_e90fcb1f091e9b4127585185e68c26f1b9d6d847e0152d264f92b621d5ec2944 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["navigation"]) ? $context["navigation"] : null);
        echo "

<div class=\"ai1ec-posterboard-view ai1ec-clearfix
\t\t\tai1ec-posterboard-";
        // line 5
        if ((isset($context["posterboard_equal_height"]) ? $context["posterboard_equal_height"] : null)) {
            echo "aligned
\t\t\t";
        } else {
            // line 6
            echo "masonry";
        }
        // line 7
        echo "\t\t\t";
        if ((!(isset($context["posterboard_footer_show"]) ? $context["posterboard_footer_show"] : null))) {
            // line 8
            echo "\t\t\t\tai1ec-posterboard-no-footer
\t\t\t";
        }
        // line 10
        echo "\t\t\t";
        if ((isset($context["has_product_buy_button"]) ? $context["has_product_buy_button"] : null)) {
            // line 11
            echo "\t\t\t\tai1ec-has-product-buy-button
\t\t\t";
        }
        // line 13
        echo "\t\t\t\"
\tdata-ai1ec-tile-min-width=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["tile_min_width"]) ? $context["tile_min_width"] : null), "html", null, true);
        echo "\"
\tdata-ai1ec-posterboard-equal-height=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["posterboard_equal_height"]) ? $context["posterboard_equal_height"] : null), "html", null, true);
        echo "\"
\tdata-ai1ec-posterboard-mode=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["posterboard_mode"]) ? $context["posterboard_mode"] : null), "html", null, true);
        echo "\">
\t";
        // line 17
        if (twig_test_empty((isset($context["dates"]) ? $context["dates"] : null))) {
            // line 18
            echo "\t\t<p class=\"ai1ec-no-results\">
\t\t\t";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["text"]) ? $context["text"] : null), "no_results"), "html", null, true);
            echo "
\t\t</p>
\t";
        } else {
            // line 22
            echo "\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["dates"]) ? $context["dates"] : null));
            foreach ($context['_seq'] as $context["date"] => $context["date_info"]) {
                // line 23
                echo "\t\t\t";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["date_info"]) ? $context["date_info"] : null), "events"));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 24
                    echo "\t\t\t\t";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["category"]) ? $context["category"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
                        // line 25
                        echo "\t\t\t\t\t<div class=\"ai1ec-event
\t\t\t\t\t\tai1ec-event-id-";
                        // line 26
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "post_id"), "html", null, true);
                        echo "
\t\t\t\t\t\tai1ec-event-instance-id-";
                        // line 27
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "instance_id"), "html", null, true);
                        echo "
\t\t\t\t\t\t";
                        // line 28
                        if ($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "is_allday")) {
                            echo "ai1ec-allday";
                        }
                        echo "\"
\t\t\t\t\t\t";
                        // line 29
                        if ((!twig_test_empty($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "ticket_url")))) {
                            // line 30
                            echo "\t\t\t\t\t\t\tdata-ticket-url=\"";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "ticket_url"), "html_attr");
                            echo "\"
\t\t\t\t\t\t";
                        }
                        // line 32
                        echo "\t\t\t\t\t\tdata-end=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "end"), "html", null, true);
                        echo "\">
\t\t\t\t\t\t<div class=\"ai1ec-event-wrap ai1ec-clearfix\">
\t\t\t\t\t\t\t<div class=\"ai1ec-date-block-wrap\"
\t\t\t\t\t\t\t\t";
                        // line 35
                        echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "category_bg_color");
                        echo ">
\t\t\t\t\t\t\t\t<a class=\"ai1ec-load-view\"
\t\t\t\t\t\t\t\t\thref=\"";
                        // line 37
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["date_info"]) ? $context["date_info"] : null), "href"), "html_attr");
                        echo "\">
\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-date\">
\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-month\">";
                        // line 39
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["date_info"]) ? $context["date_info"] : null), "month"), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-day\">";
                        // line 40
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["date_info"]) ? $context["date_info"] : null), "day"), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-weekday\">";
                        // line 41
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["date_info"]) ? $context["date_info"] : null), "weekday"), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                        // line 42
                        if ((isset($context["show_year_in_agenda_dates"]) ? $context["show_year_in_agenda_dates"] : null)) {
                            // line 43
                            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-year\">";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["date_info"]) ? $context["date_info"] : null), "year"), "html", null, true);
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 45
                        echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                        // line 46
                        if ($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "is_multiday")) {
                            // line 47
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ((isset($context["posterboard_equal_height"]) ? $context["posterboard_equal_height"] : null)) {
                                // line 48
                                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-end-date-divider-small ai1ec-end-date-divider\"
\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 49
                                echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "category_divider_color");
                                echo ">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-end-date-divider\"
\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 52
                                echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "category_divider_color");
                                echo ">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-end-date ai1ec-date\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-date\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-month\">";
                                // line 56
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "month"), "html", null, true);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-day\">";
                                // line 57
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "day"), "html", null, true);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-weekday\">";
                                // line 58
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "weekday"), "html", null, true);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 59
                                if ((isset($context["show_year_in_agenda_dates"]) ? $context["show_year_in_agenda_dates"] : null)) {
                                    // line 60
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-year\">";
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "year"), "html", null, true);
                                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 62
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                            } else {
                                // line 65
                                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-end-date\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 66
                                if (($this->getAttribute((isset($context["date_info"]) ? $context["date_info"] : null), "month") != $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "month"))) {
                                    // line 67
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-month\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-end-date-divider\"
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 69
                                    echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "category_divider_color");
                                    echo "></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 70
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "month"), "html", null, true);
                                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 72
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-end-date-divider\"
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 73
                                    echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "category_divider_color");
                                    echo ">
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 76
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-day\">";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "day"), "html", null, true);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-weekday\">";
                                // line 77
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "weekday"), "html", null, true);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 78
                                if ((isset($context["show_year_in_agenda_dates"]) ? $context["show_year_in_agenda_dates"] : null)) {
                                    // line 79
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-year\">";
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "enddate_info"), "year"), "html", null, true);
                                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 81
                                echo "\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 83
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 84
                        echo "\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t";
                        // line 87
                        $context["edit_post_link"] = $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "edit_post_link");
                        // line 88
                        echo "\t\t\t\t\t\t\t";
                        if ((!twig_test_empty((isset($context["edit_post_link"]) ? $context["edit_post_link"] : null)))) {
                            // line 89
                            echo "\t\t\t\t\t\t\t\t<a class=\"post-edit-link\" href=\"";
                            echo (isset($context["edit_post_link"]) ? $context["edit_post_link"] : null);
                            echo "\">
\t\t\t\t\t\t\t\t\t<i class=\"ai1ec-fa ai1ec-fa-pencil\"></i> ";
                            // line 90
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["text"]) ? $context["text"] : null), "edit"), "html", null, true);
                            echo "
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
                        }
                        // line 93
                        echo "
\t\t\t\t\t\t\t<div class=\"ai1ec-event-title-wrap ai1ec-clearfix\">
\t\t\t\t\t\t\t\t<div class=\"ai1ec-event-title\"
\t\t\t\t\t\t\t\t\ttitle=\"";
                        // line 96
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "filtered_title"), "html_attr");
                        echo "\"><div>
\t\t\t\t\t\t\t\t\t<a class=\"ai1ec-load-event\"
\t\t\t\t\t\t\t\t\t\thref=\"";
                        // line 98
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "permalink"), "html_attr");
                        echo "\"
\t\t\t\t\t\t\t\t\t  ";
                        // line 99
                        echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "category_text_color");
                        echo ">
\t\t\t\t\t\t\t\t\t\t";
                        // line 100
                        echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "filtered_title");
                        echo "
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t";
                        // line 102
                        if (((!(isset($context["posterboard_equal_height"]) ? $context["posterboard_equal_height"] : null)) && (!twig_test_empty($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "venue"))))) {
                            // line 103
                            echo "\t\t\t\t\t\t\t\t\t\t<span class=\"ai1ec-event-location\">@ ";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "venue"), "html", null, true);
                            echo "</span>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 105
                        echo "\t\t\t\t\t\t\t\t</div></div>

\t\t\t\t\t\t\t\t<div class=\"ai1ec-event-time\">
\t\t\t\t\t\t\t\t\t";
                        // line 108
                        if (((isset($context["is_ticket_button_enabled"]) ? $context["is_ticket_button_enabled"] : null) && (!twig_test_empty($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "ticket_url"))))) {
                            // line 109
                            echo "\t\t\t\t\t\t\t\t\t\t<a class=\"ai1ec-pull-right ai1ec-btn ai1ec-btn-primary
\t\t\t\t\t\t\t\t\t\t\t\tai1ec-btn-xs ai1ec-buy-tickets\"
\t\t\t\t\t\t\t\t\t\t\ttarget=\"_blank\" href=\"";
                            // line 111
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "ticket_url"), "html", null, true);
                            echo "\"
\t\t\t\t\t\t\t\t\t\t\t>";
                            // line 112
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "ticket_url_label"), "html", null, true);
                            echo "</a>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 114
                        echo "\t\t\t\t\t\t\t\t\t ";
                        echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "timespan_short");
                        echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"ai1ec-posterboard-image-container\">
\t\t\t\t\t\t\t\t<a class=\"ai1ec-load-event\"
\t\t\t\t\t\t\t\t\thref=\"";
                        // line 119
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "permalink"), "html_attr");
                        echo "\">
\t\t\t\t\t\t\t\t\t";
                        // line 120
                        if ((isset($context["posterboard_equal_height"]) ? $context["posterboard_equal_height"] : null)) {
                            // line 121
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-posterboard-image\"
\t\t\t\t\t\t\t\t\t\t\t style=\"background-image: url(";
                            // line 122
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "avatar_url"), "html_attr");
                            // line 123
                            echo ");\">
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 126
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "avatar_not_wrapped");
                            echo "
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 128
                        echo "\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<div class=\"ai1ec-posterboard-time\">
\t\t\t\t\t\t\t\t\t";
                        // line 130
                        echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "short_start_time");
                        echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                        // line 132
                        echo (isset($context["action_buttons"]) ? $context["action_buttons"] : null);
                        echo "
\t\t\t\t\t\t\t\t";
                        // line 133
                        if ((!twig_test_empty($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "post_excerpt")))) {
                            // line 134
                            echo "\t\t\t\t\t\t\t\t\t<div class=\"ai1ec-event-description\">
\t\t\t\t\t\t\t\t\t\t";
                            // line 135
                            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "post_excerpt");
                            echo "
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                        }
                        // line 138
                        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                        // line 139
                        if (((!twig_test_empty($this->getAttribute((isset($context["event"]) ? $context["event"] : null), "venue"))) && (isset($context["posterboard_equal_height"]) ? $context["posterboard_equal_height"] : null))) {
                            // line 140
                            echo "\t\t\t\t\t\t\t\t<span class=\"ai1ec-event-location\">
\t\t\t\t\t\t\t\t\t<i class=\"ai1ec-fa ai1ec-fa-map-marker\"></i>
\t\t\t\t\t\t\t\t\t";
                            // line 142
                            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "venue");
                            echo "
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t";
                        }
                        // line 145
                        echo "\t\t\t\t\t\t\t";
                        $context["categories"] = $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "categories_html");
                        // line 146
                        echo "\t\t\t\t\t\t\t";
                        $context["tags"] = $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "tags_html");
                        // line 147
                        echo "\t\t\t\t\t\t\t";
                        if ((isset($context["posterboard_footer_show"]) ? $context["posterboard_footer_show"] : null)) {
                            // line 148
                            echo "\t\t\t\t\t\t\t\t";
                            if ((((!twig_test_empty((isset($context["categories"]) ? $context["categories"] : null))) || (!twig_test_empty((isset($context["tags"]) ? $context["tags"] : null)))) || (isset($context["posterboard_equal_height"]) ? $context["posterboard_equal_height"] : null))) {
                                // line 149
                                echo "\t\t\t\t\t\t\t\t\t<footer>
\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t";
                                // line 151
                                if ((!twig_test_empty((isset($context["categories"]) ? $context["categories"] : null)))) {
                                    // line 152
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"ai1ec-categories\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 153
                                    echo (isset($context["categories"]) ? $context["categories"] : null);
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 156
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                                if ((!twig_test_empty((isset($context["tags"]) ? $context["tags"] : null)))) {
                                    // line 157
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"ai1ec-tags\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                    // line 158
                                    echo (isset($context["tags"]) ? $context["tags"] : null);
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 161
                                echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t";
                            }
                            // line 164
                            echo "\t\t\t\t\t\t\t";
                        }
                        // line 165
                        echo "\t\t\t\t\t\t\t";
                        if ((!(isset($context["posterboard_footer_show"]) ? $context["posterboard_footer_show"] : null))) {
                            // line 166
                            echo "\t\t\t\t\t\t\t\t<div class=\"ai1ec-no-footer-padding\"></div>
\t\t\t\t\t\t\t";
                        }
                        // line 168
                        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 170
                    echo " ";
                    // line 171
                    echo "\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " ";
                // line 172
                echo "\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['date'], $context['date_info'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " ";
            // line 173
            echo "\t";
        }
        echo " ";
        // line 174
        echo "</div>
<div class=\"ai1ec-pull-left\">";
        // line 175
        echo (isset($context["pagination_links"]) ? $context["pagination_links"] : null);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "posterboard.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  477 => 175,  474 => 174,  470 => 173,  463 => 172,  456 => 171,  454 => 170,  446 => 168,  442 => 166,  439 => 165,  436 => 164,  431 => 161,  425 => 158,  422 => 157,  419 => 156,  413 => 153,  410 => 152,  408 => 151,  404 => 149,  401 => 148,  398 => 147,  395 => 146,  392 => 145,  386 => 142,  382 => 140,  380 => 139,  377 => 138,  371 => 135,  368 => 134,  366 => 133,  362 => 132,  357 => 130,  353 => 128,  347 => 126,  342 => 123,  340 => 122,  337 => 121,  335 => 120,  331 => 119,  322 => 114,  317 => 112,  313 => 111,  309 => 109,  307 => 108,  302 => 105,  296 => 103,  294 => 102,  289 => 100,  285 => 99,  281 => 98,  276 => 96,  271 => 93,  265 => 90,  260 => 89,  257 => 88,  255 => 87,  250 => 84,  247 => 83,  243 => 81,  237 => 79,  235 => 78,  231 => 77,  226 => 76,  220 => 73,  217 => 72,  212 => 70,  208 => 69,  204 => 67,  202 => 66,  199 => 65,  194 => 62,  188 => 60,  186 => 59,  182 => 58,  178 => 57,  174 => 56,  167 => 52,  161 => 49,  158 => 48,  155 => 47,  153 => 46,  150 => 45,  144 => 43,  142 => 42,  138 => 41,  134 => 40,  130 => 39,  125 => 37,  120 => 35,  113 => 32,  107 => 30,  105 => 29,  99 => 28,  95 => 27,  91 => 26,  88 => 25,  83 => 24,  78 => 23,  73 => 22,  67 => 19,  64 => 18,  36 => 8,  33 => 7,  30 => 6,  38 => 7,  25 => 5,  52 => 12,  47 => 13,  41 => 8,  35 => 6,  27 => 4,  60 => 13,  48 => 8,  40 => 10,  34 => 5,  29 => 4,  22 => 2,  44 => 10,  28 => 4,  24 => 4,  86 => 26,  75 => 21,  70 => 19,  66 => 14,  62 => 17,  58 => 16,  54 => 15,  50 => 14,  43 => 11,  39 => 7,  31 => 5,  26 => 3,  21 => 2,  19 => 1,);
    }
}
