��          �      |      �     �     �  #        *     F  !   f  �   �     +     1     6     <  A   D     �     �  
   �  
   �  #   �  "   �  
   �        6     �  R     �     �     �  
     
        )  �   7     �  
   �     �  	   �  H     
   J  
   U  
   `     k  #   y  -   �     �     �  6   �                           	                                
                                      Back to cart Billing Frontend: button labelBack to cart Frontend: button labelNext Frontend: button labelPrevious Frontend: button labelSkip Login If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing &amp; Shipping section. Login Next Order Payment Please fix the errors on this step before moving to the next step Previous Shipping SilkyPress Skip Login WooCommerce Multi-Step Checkout Pro You must be logged in to checkout. Your order https://www.silkypress.com https://www.silkypress.com/wp-multi-step-checkout-pro/ Project-Id-Version: WooCommerce Multi-Step Checkout Pro 1.17.2
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-multi-step-checkout-pro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-06-10 19:33+0200
Language-Team: 
X-Generator: Poedit 2.2.1
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl
 Ritorna al carrello Fatturazione Ritorna al carrello Successivo Precedente Salta accesso Se hai precedentemente acquistato da noi, inserisci i tuoi dettagli nei campi qui sotto. Se sei un nuovo cliente, procedi alla sezione Fatturazione &amp; spedizione. Accesso Successivo Ordine Pagamento Correggi gli errori in questo salto prima di passare al salto successivo Precedente Spedizione SilkyPress Salta accesso WooCommerce Multi-Step Checkout Pro Devi essere connesso per completare l'ordine. Il tuo ordine https://www.silkypress.com https://www.silkypress.com/wp-multi-step-checkout-pro/ 