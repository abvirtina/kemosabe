��            )   �      �     �     �  #   �     �     �  !     �   (     �  
   �     �  3   �               #     ,     5  )   >  J   h  G   �  #   �  
     
   *  [   5  #   �  "   �  
   �     �  6   �  �  5     �     �     �     �            �   ,  	   �     �     	  =   	     J	     S	     \	  	   h	  	   r	  %   |	  =   �	  8   �	     
  
   9
     D
  ^   ]
  #   �
  +   �
            6   6                            
                	                                                                                              Back to cart Billing Frontend: button labelBack to cart Frontend: button labelNext Frontend: button labelPrevious Frontend: button labelSkip Login If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing &amp; Shipping section. Login Main Color Next Nice multi-step checkout for your WooCommerce store Order Payment Previous Settings Shipping Show the <code>Back to Cart</code> button Show the <code>Billing</code> and the <code>Shipping</code> steps together Show the <code>Order</code> and the <code>Payment</code> steps together Show the <code>Shipping</code> step SilkyPress Skip Login The WP Multi-Step Checkout plugin is enabled, but it requires WooCommerce in order to work. WooCommerce Multi-Step Checkout Pro You must be logged in to checkout. Your order https://www.silkypress.com https://www.silkypress.com/wp-multi-step-checkout-pro/ Project-Id-Version: Plugins - WooCommerce Multi-Step Checkout - Development (trunk)
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-multi-step-checkout-pro
PO-Revision-Date: 2018-12-12 20:06+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 2.2
 Retour au panier Facturation Retour au panier Suivant Précédent Ignorer l'Identification Si vous avez déjà commandé avec nous auparavant, veuillez saisir vos coordonnées ci-dessous. Si vous êtes un nouveau client, veuillez renseigner la section facturation et livraison. Connexion Couleur principale Suivant Agréable multi-step checkout pour votre boutique WooCommerce Commande Paiement Précédent Réglages Livraison Afficher le bouton "Retour au panier" Afficher ensemble les étapes de Facturation et d'Expédition Afficher ensemble la Commande et les étapes de Paiement Afficher l'étape d'Expédition SilkyPress Ignorer l'Identification Le plugin WP Multi-Step Checkout est activé, mais il nécessite WooCommerce pour fonctionner. WooCommerce Multi-Step Checkout Pro Vous devez être identifié pour commander. Votre commande https://www.silkypress.com https://www.silkypress.com/wp-multi-step-checkout-pro/ 