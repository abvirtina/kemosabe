��            )         �     �     �     �  #   �     �       !   .  �   P     �  
   �            3        M     S     [     d     m  )   v  J   �  G   �  #   3  
   W  
   b     m  #   v  "   �  
   �     �  6   �  �       �     �  	   �     �     �     �     �  �        �     �     �     �  /   �  
   �     	     	     	  
   %	  8   0	  1   i	  /   �	     �	  
   �	     �	     
  #   
     /
     L
     Z
  6   u
                                            
                                                          	                                  Back to cart Billing Default Frontend: button labelBack to cart Frontend: button labelNext Frontend: button labelPrevious Frontend: button labelSkip Login If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing &amp; Shipping section. Login Main Color Material Design Next Nice multi-step checkout for your WooCommerce store Order Payment Previous Settings Shipping Show the <code>Back to Cart</code> button Show the <code>Billing</code> and the <code>Shipping</code> steps together Show the <code>Order</code> and the <code>Payment</code> steps together Show the <code>Shipping</code> step SilkyPress Skip Login Template WooCommerce Multi-Step Checkout Pro You must be logged in to checkout. Your order https://www.silkypress.com https://www.silkypress.com/wp-multi-step-checkout-pro/ Project-Id-Version: WooCommerce Multi-Step Checkout Pro 1.17.2
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-multi-step-checkout-pro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-06-10 19:33+0200
Language-Team: 
X-Generator: Poedit 2.2.1
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl
 Terug naar winkelwagen Facturering Standaard Terug naar winkelwagen Volgende Vorige Inloggen overslaan Als je eerder bij ons hebt gewinkeld, vul dan hieronder je gegevens in. Als je een nieuwe klant bent, ga dan door naar facturering en verzending. Inloggen Basis kleur Material Design Volgende Leuke Mult-step Kassa voor u WooCommerce winkel Bestelling Betaling Vorige Instellingen Verzending Toon de <code>terug naar winkelwagententje</code> button Toon de Facturatie en Verzending steppen tegelijk Toon de Bestelling en Betaling stappen tegelijk Toon de verzendings stap SilkyPress Inloggen overslaan Template WooCommerce Multi-Step Checkout Pro U moet inloggen in de kassa. Uw bestelling https://www.silkypress.com https://www.silkypress.com/wp-multi-step-checkout-pro/ 