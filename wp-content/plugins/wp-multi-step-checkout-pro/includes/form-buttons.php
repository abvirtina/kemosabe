<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$button_class = (isset($options['wpmc_buttons']) && $options['wpmc_buttons'] == '1' ) ? 'wpmc-button' : 'button alt';

?>

<!-- The steps buttons -->
<div class="wpmc-nav-wrapper">
    <div class="wpmc-footer-left">
        <?php if( $options['show_back_to_cart_button'] ) : ?>
          <button data-href="<?php echo wc_get_cart_url(); ?>" id="wpmc-back-to-cart" class="<?php echo $button_class; ?>" type="button"><?php echo $options['t_back_to_cart']; ?></button>
        <?php endif; ?>
    </div>
    <div class="wpmc-footer-right wpmc-nav-buttons">
        <button id="wpmc-prev" class="<?php echo $button_class; ?> button-inactive" type="button"><?php echo $options['t_previous']; ?></button>
        <?php if ( $show_login_step ) : ?>
            <button id="wpmc-next" class="<?php echo $button_class; ?> button-active" type="button"><?php echo $options['t_next']; ?></button>
            <button id="wpmc-skip-login" class="<?php echo $button_class; ?> button-active current" type="button"><?php echo $options['t_skip_login']; ?></button>
        <?php else : ?>
            <button id="wpmc-next" class="<?php echo $button_class; ?> button-active current" type="button"><?php echo $options['t_next']; ?></button>
        <?php endif; ?>
    </div>
</div>
