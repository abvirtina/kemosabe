var offline = '', 
onlineOrders = 0, 
revenue = 0,
visitors = 0,
orderCount = 0,
visitors = 0,
offOrders = 0,
avgOrder = 0,
convRate = 0,
spend = 0,
projectedRevenue = 0,
projectedGProfit = 0,
rdmSpend = 0,
firstYrInvstmnt = 0,
fieldVals = [];

jv('#offline').click(function(){
    if(jv('#offline').prop('checked')){
        jv('.offline').slideDown('fast');
        offline = true;
    }
    else{
        jv('.offline').slideUp('fast');
        offline = false;
    }
});

jv('#roiForm .field input').keyup(function(){
    var name = jv(this).attr('name'),
    value = jv(this).val();
    fieldVals['revenue'] = jv('input[name="revenue"]').val(),
    fieldVals['visitors'] = jv('input[name="visitors"]').val(),
    fieldVals['orderCount'] = jv('input[name="order-count"]').val(),
    fieldVals['grossProfit'] = jv('input[name="gross-profit"]').val(),
    fieldVals['offOrders'] = jv('input[name="offline-orders"]').val(),
    fieldVals['spend'] = jv('input[name="spend"]').val(),
    fieldVals['roAvgOrder'] = jv('input[name="order-size"]').val(),
    fieldVals['roVisitors'] = jv('input[name="visitors-re"]').val(),
    fieldVals['roConvRate'] = jv('input[name="conversion-rate"]').val(),
    fieldVals['platformInvest'] = jv('input[name="platform-invest"]').val();
    fnCalculate(fieldVals);
});

function fnCalculate(fieldVals){
    avgOrder = fieldVals['revenue'] / fieldVals['orderCount'];
    jv('.order-size strong').text('$' + avgOrder);
    
    onlineOrders = fieldVals['orderCount'] * (1 - fieldVals['offOrders'] / 100);            
    jv('.online-orders strong').text(onlineOrders);

    convRate = (onlineOrders / fieldVals['visitors']) * 100;
    jv('.conversion-rate strong').text(convRate.toFixed(2) + '%');

    onlineRevenue = avgOrder * onlineOrders;
    jv('.onine-revenue strong').text('$' + onlineRevenue.toFixed(2));

    grossProfit = onlineRevenue * (fieldVals['grossProfit'] / 100);
    jv('.gross-profit strong').text('$' + grossProfit.toFixed(2));

    roAvgOrder  = avgOrder * (1 + fieldVals['roAvgOrder'] / 100);
    jv('.roi-order-size .outcome').text('$' + roAvgOrder.toFixed(2));
    roVisitors  = fieldVals['visitors'] * (1 + fieldVals['roVisitors'] / 100);
    jv('.roi-visitor .outcome').text(roVisitors);

    roConvRate  = convRate * (1 + fieldVals['roConvRate'] / 100);
    jv('.roi-conv-rate .outcome').text(roConvRate.toFixed(2) + '%');

    projectedRevenue = roAvgOrder * roVisitors * (roConvRate / 100);
    jv('.projected-revenue strong').text('$' + projectedRevenue.toFixed(2));

    projectedGProfit = projectedRevenue * (fieldVals['grossProfit'] / 100);
    jv('.projected-gross-profit strong').text('$' + projectedGProfit.toFixed(2));

    liftProfit = projectedGProfit - grossProfit;
    jv('.list-profit strong').text('$' + liftProfit.toFixed(2));

    rdmSpend = fieldVals['revenue'] * (fieldVals['spend'] / 100);
    jv('.rdms strong').text('$' + rdmSpend.toFixed(2));

    firstYrInvstmnt = parseInt(fieldVals['platformInvest']) +(rdmSpend * 6) + (12-6) * rdmSpend ;
    jv('.fyi strong').text('$' + firstYrInvstmnt.toFixed(2));

    firstYrNetProfInc = (12 - 6)*liftProfit - firstYrInvstmnt;
    jv('.fypi strong').text('$' + firstYrNetProfInc.toFixed(2));

}


jv('input[name="hitconversion"]').focusout(function(){
    if(jv(this).val() < 3){
        jv(this).parents('.field').find('#waring-alert').show();
        jv(this).val(3);
    }
});

jv('input[name=totrevenue], input[name=moncroinvest]').focusout(function(){
    if(jv(this).val() > 0){
        var num = digits(jv(this).val());
        jv(this).val('$'+num);
    }
});

jv('input[name=totrevenue], input[name=moncroinvest]').focus(function(){
    if(jv(this).val() != ''){
        var num = jv(this).val().replace('$', '');
        num = num.replace(',', '');
        jv(this).val(num);
    }
});

jv('input[name=visitors], input[name=numorders]').focusout(function(){
    if(jv(this).val() > 0){
        var num = digits(jv(this).val());
        jv(this).val(num);
    }
});

jv('input[name=visitors], input[name=numorders]').focus(function(){
    if(jv(this).val() != ''){
        var num = jv(this).val().replace(',', '');
        jv(this).val(num);
    }
});

function digits(digit){ 
    return digit.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ; 
}

jv('#croForm .field input').keyup(function(){


    if(jv('select[name="duration"]').val()!='' && jv('input[name="totrevenue"]').val()!='' && jv('input[name="visitors"]').val()!=''  && jv('input[name="numorders"]').val()!='') {
        
        var duration = jv('select[name="duration"]').val();
        var totrevenue = jv('input[name="totrevenue"]').val().replace(',', '');
        totrevenue = totrevenue.replace('$', '');
        var visitors = jv('input[name="visitors"]').val().replace(',', '');
        var numorders = jv('input[name="numorders"]').val().replace(',', '');
        var monthcro = jv('input[name="moncro"]').val();
        var moncroinvest = jv('input[name="moncroinvest"]').val().replace('$', '');
        //alert(moncroinvest);
        if(moncroinvest=='') {
            moncroinvest = 10;
        } else {
            moncroinvest = moncroinvest.replace(',', '');
        } 
        //alert(moncroinvest);     
        var hitconversion = jv('input[name="hitconversion"]').val();
        var totinvestment = jv('input[name="totinvestment"]').val();
        var estlift = jv('input[name="estlift"]').val();


        var avgordval = totrevenue/numorders;
        var montraffic = visitors/duration;
        var monthrev = totrevenue/duration;
        var monthorder = numorders/duration;
        var conversionrate = ( monthorder/montraffic ) * 100;
        var totinvestment = moncroinvest * hitconversion;


        var newconrate = parseFloat(estlift)+parseFloat(conversionrate);
        var avgmonord = (montraffic*newconrate) / 100;
        var newrev = avgmonord*avgordval;
        var lifmonrev = parseFloat(newrev)-parseFloat(monthrev);

        jv('.order-value strong').html('$'+avgordval.toFixed(2));
        jv('.monthly-traffic strong').html(montraffic.toFixed(2));
        jv('.monthly-revenue strong').html('$'+monthrev.toFixed(2));
        jv('.monthly-orders strong').html(monthorder.toFixed(2));
        jv('.curConversion').text(conversionrate.toFixed(2)+'%');
        jv('.total-investment strong').html('$'+digits(Math.ceil(totinvestment)));
        jv('.newconrate strong').html(newconrate.toFixed(2)+'%');
        jv('.avgmonord strong').html(Math.ceil(avgmonord));
        jv('.newrev strong').html('$'+digits(Math.floor(newrev)));
        jv('.lifmonrev strong').html('$'+digits(Math.floor(lifmonrev)));

        var sixmonth = ( lifmonrev * 6 ) - totinvestment;
        var twlvmonth = ( lifmonrev * 12 ) - totinvestment;
        var eigtmonth = ( lifmonrev * 18 ) - totinvestment;
        var twefrmonth = ( lifmonrev * 24 ) - totinvestment;
        var thrtsixmonth = ( lifmonrev * 36 ) - totinvestment;

        jv('.sixmonth').html('$'+sixmonth.toFixed(2));
        jv('.twlvmonth').html('$'+twlvmonth.toFixed(2));
        jv('.eigtmonth').html('$'+eigtmonth.toFixed(2));
        jv('.twefrmonth').html('$'+twefrmonth.toFixed(2));
        jv('.thrtsixmonth').html('$'+thrtsixmonth.toFixed(2));


        var sixmonthroi = sixmonth / totinvestment;
        var twlvmonthroi = twlvmonth / totinvestment;
        var eigtmonthroi = eigtmonth / totinvestment;
        var twefrmonthroi = twefrmonth / totinvestment;
        var thrtsixmonthroi = thrtsixmonth / totinvestment;      


            jv('.sixmonthroi').html(Math.round(sixmonthroi)+' X');
            jv('.twlvmonthroi').html(Math.round(twlvmonthroi)+' X');
            jv('.eigtmonthroi').html(Math.round(eigtmonthroi)+' X');
            jv('.twefrmonthroi').html(Math.round(twefrmonthroi)+' X');
            jv('.thrtsixmonthroi').html(Math.round(thrtsixmonthroi)+' X');   
        

    }

});
jv('select[name="duration"]').change(function(){
    jv('.sOpMonth').text(jv(this).val());
});


jv('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
jv('.quantity').each(function() {
    var spinner = jv(this),
    input = spinner.find('input[type="number"]'),
    btnUp = spinner.find('.quantity-up'),
    btnDown = spinner.find('.quantity-down'),
    min = input.attr('min'),
    max = input.attr('max');

    btnUp.click(function() {
    var oldValue = parseFloat(input.val());
    if (oldValue >= max) {
        var newVal = oldValue;
    } else {
        var newVal = oldValue + 0.25;
    }
    spinner.find("input").val(newVal);
    spinner.find("input").trigger("change");
    spinner.find("input").trigger("keyup");
    });

    btnDown.click(function() {
    var oldValue = parseFloat(input.val());
    if (oldValue <= min) {
        var newVal = oldValue;
    } else {
        var newVal = oldValue - 0.25;
    }
    spinner.find("input").val(newVal);
    spinner.find("input").trigger("change");
    spinner.find("input").trigger("keyup");
    });

});

jv('.togglepanel').click(function(e){
    e.preventDefault();
    jv(this).parents('.accordian').find('.accordianPanel').removeClass('active');
    jv(this).parent().addClass('active');
    jv('.accordian .accordianPanel .accordianContent').slideUp('fast', 'swing');
    jv('.accordian .accordianPanel.active .accordianContent').slideDown('fast', 'swing');
});

jv('.navigate .next').click(function(e){
    e.preventDefault();
    jv(this).parents('.accordianPanel').removeClass('active');
    jv(this).parents('.accordianContent').slideUp('fast', 'swing');
    jv(this).parents('.accordianPanel').next('.accordianPanel').addClass('active');
    jv('.accordian .accordianPanel.active .accordianContent').slideDown('fast', 'swing');
});

jv('.navigate .prev').click(function(e){
    e.preventDefault();
    jv(this).parents('.accordianPanel').removeClass('active');
    jv(this).parents('.accordianContent').slideUp('fast', 'swing');
    jv(this).parents('.accordianPanel').prev('.accordianPanel').addClass('active');
    jv('.accordian .accordianPanel.active .accordianContent').slideDown('fast', 'swing');
});

jv('.step1 .next').click(function(){
    var rate = jv(this).parents('.accordianContent').find('.bBottom h4 span').text();
    jv(this).parents('.accordianPanel').find('.togglepanel span').text('Current Conversion '+rate);
});

jv('.step3 .prev').click(function(){
    var rate = jv(this).parents('.accordianContent').find('.bBottom .total-investment strong').text();
    jv(this).parents('.accordianPanel').find('.togglepanel span').text(rate);
});

jv('.step2 .next, .step2 .prev').click(function(){
    var rate = jv(this).parents('.accordianContent').find('.bBottom .newconrate strong').text();
    jv(this).parents('.accordianPanel').find('.togglepanel span').text('New Conversion '+rate);
});
//$.isNumeric( "3.1415" )



jv('.mainitemview').click(function(){
    alert('clicked');
});